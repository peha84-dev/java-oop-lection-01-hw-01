package lection.one.hw.one;

public class Main {

    public static void main(String[] args) {
        Product productOne = new Product(15999.99, 64, "Холодильник INDESIT LI8S1ES");
        Product productTwo = new Product(5599.00, 13.5, "Мікрохвильова піч SAMSUNG MS23F302TAS/UA");

        System.out.println(productOne);
        System.out.println(productTwo);
    }
}